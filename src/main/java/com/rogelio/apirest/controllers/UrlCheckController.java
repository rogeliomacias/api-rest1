package com.rogelio.apirest.controllers;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UrlCheckController {
    private final String SITE_IS_UP = "sitio arriba";
    private final String SITE_IS_DOWN = "sitio abajo!!";
    private final String INCORRECT_URL = "sitio incorrecto";

    @GetMapping("/check")
    public String getUrlStatusMessage(@RequestParam String url) {
        String returnMessage = "";
        try {
            URL urlObj = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            int responseCodeCat = conn.getResponseCode() / 100;
            System.out.println(responseCodeCat);
            if (responseCodeCat != 2 && responseCodeCat != 3) {
                returnMessage = SITE_IS_DOWN;
            } else {
                returnMessage = SITE_IS_UP;
            }
        } catch (MalformedURLException e) {
            
            //e.printStackTrace();
            returnMessage = INCORRECT_URL;
        } catch (IOException e) {
            
            returnMessage = SITE_IS_DOWN;
            //e.printStackTrace();
        }

        return returnMessage;
    }
}
